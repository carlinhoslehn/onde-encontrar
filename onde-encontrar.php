<?php
/**
 * Plugin Name: Onde Encontrar
 * Plugin URI: http://bradigital.com.br
 * Description: Plugin usado para cadastro de lojas onde encontrar e comercio exterior
 * Version: 0.0.1
 * Author: Carlos Eduardo Lehn
 * Author URI: http://carlinhoslehn.com
 * License: GPL2
 */

define( 'OE_VERSION', '0.0.1' );
define( 'OE_WP_VERSION', get_bloginfo( 'version' ) );
define( 'OE_PLUGIN', __FILE__ );
define( 'OE_PLUGIN_BASENAME', plugin_basename( OE_PLUGIN ) );

if ( is_admin() ) {
    require_once (OE_PLUGIN.'/inc/class.admin.php');
}else{
    require_once (OE_PLUGIN.'/inc/class.front.php');
}

