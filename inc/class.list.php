<?php
//https://www.smashingmagazine.com/2011/11/native-admin-tables-wordpress/

if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class table_onde_encontrar extends WP_List_Table {

    public $data = array();
    public $columns = array();
    public $sortable_columns = array();
    public $table = null;

    function __construct(){

        global $status, $page;
                
        //Set parent defaults
        parent::__construct( array(
            'singular'  => 'id',     //singular name of the listed records
            'plural'    => 'ids',    //plural name of the listed records
            'ajax'      => false        //does this table support ajax?
        ) );
        
    }

    function column_default($item, $column_name){
        switch($column_name){

            case 'id':
                return $item[$column_name];
            default:
                return $item[$column_name];
        }
    }

    function column_name($item) {
        
        //Build row actions
        $actions = array(
            'edit'      => sprintf('<a href="?page=%s&action=%s&id=%s">Edit</a>','oe_manage_'.$this->table,'edit',$item['id']),
            'delete'    => sprintf('<a href="?page=%s&action=%s&id=%s">Delete</a>','oe_manage_'.$this->table,'delete',$item['id']),
        );
        
        //Return the title contents
        return sprintf('%1$s <span style="color:silver"></span>%2$s',
            /*$2%s*/ $item['name'],
            /*$3%s*/ $this->row_actions($actions)
        );
    }

    function column_cb($item){
        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            /*$1%s*/ $this->_args['singular'],  //Let's simply repurpose the table's singular label ("movie")
            /*$2%s*/ $item['id']                //The value of the checkbox should be the record's id
        );
    }

    function get_columns(){

        return $this->columns;
    }

    function get_sortable_columns() {

        return $this->sortable_columns;
    }

    function get_bulk_actions() {
        $actions = array(
            'delete'    => 'Delete',
            'inative'    => 'Inative all'
        );
        return $actions;
    }

    function process_bulk_action() {
        
        //Detect when a bulk action is being triggered...
        if( 'delete'===$this->current_action() ) {
            wp_die('Items deleted (or they would be if we had items to delete)!');
        }
        
    }

    function prepare_items() {
        global $wpdb;

        $per_page = 5;

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->process_bulk_action();

        $query = "SELECT * FROM ".$wpdb->prefix.'onde_encontrar_'.$this->table;

        /* -- Ordering parameters -- */
        //Parameters that are going to be used to order the result
        $orderby = !empty($_GET["orderby"]) ? sanitize_text_field($_GET["orderby"]) : 'ASC';
        $order = !empty($_GET["order"]) ? sanitize_text_field($_GET["order"]) : '';
        if(!empty($orderby) & !empty($order)){ $query.=' ORDER BY '.$orderby.' '.$order; }

        $total_items = $wpdb->query($query);

        //Which page is this?
        $paged = $this->get_pagenum();

        //Page Number
        if(empty($paged) || !is_numeric($paged) || $paged<=0 ){ $paged=1; }

        if(!empty($paged) && !empty($per_page)){
            $offset=($paged-1)*$per_page;
            $query.=' LIMIT '.(int)$offset.','.(int)$per_page;
        }

        $this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil($total_items/$per_page)   //WE have to calculate the total number of pages
        ) );

        /* -- Fetch the items -- */
        $item = $wpdb->get_results($query);

        //return if empty
        if(empty($item)) :
            $this->items = array();
        endif;

        $items = array();
        foreach($item as $record) {
            $record = (array)$record;
            $items[] = $record;
        }
        $this->items = $items;
    }
}