<?php

/**
 * Created by PhpStorm.
 * User: programador
 * Date: 12/8/16
 * Time: 6:20 PM
 */



if( !class_exists('oe') ):

    class oe
    {
        private $icon_plugin    = 'dashicons-search';
        private $slug_plugin    = 'oe_lojas';
        private $menu_slug      = 'oe_list';
        private $message        ='';
        private $apiKeyGoogle   = 'AIzaSyBq9pboRuHN4sPRzq7T97YZi3pTCSQNmRo';

        function __construct()
        {
            // admin only
            if( is_admin() )
            {
                
                

                add_action( 'oe_sucess', array($this,'oe_notice__success' ));
                add_action( 'oe_error', array($this,'oe_notice__error' ));
                add_action('wp_ajax_oe_combo_city',array($this,'oe_combo_city'));
                add_action('wp_ajax_oe_combo_state',array($this,'oe_combo_state'));

                add_action('oe_manage_city',array($this,'oe_manage_city'));
                add_action('oe_manage_state',array($this,'oe_manage_state'));
                add_action('oe_manage_country',array($this,'oe_manage_country'));
            }
            add_action('wp_ajax_oe_combo_store',array($this,'oe_combo_list_store'));
            add_shortcode('oe_display_lojas', array($this,'oe_shortcode'));
        }


        

        

        

        

        
    }

    $oe = new oe();

endif; //check class not exists