<?php

class Admin
{

    private $slug_plugin    = 'oe_lojas';
    private $menu_slug      = 'oe_list';
    private $message        ='';
    private $apiKeyGoogle   = 'AIzaSyBq9pboRuHN4sPRzq7T97YZi3pTCSQNmRo';

    public function __construct()
    {
        $this->add_action();

    }

    public function add_action() {

        add_action( 'admin_init', 'oe_admin_init' );
        register_activation_hook( __FILE__, array($this,'oe_install' ));
        add_action( 'admin_menu', array($this,'admin_menu'));
        add_action('admin_enqueue_scripts',array($this,'admin_css'));
        add_action('admin_enqueue_scripts',array($this,'admin_js'));

        add_action( 'oe_sucess', array($this,'oe_notice__success' ));
        add_action( 'oe_error', array($this,'oe_notice__error' ));
        add_action('wp_ajax_oe_combo_city',array($this,'oe_combo_city'));
        add_action('wp_ajax_oe_combo_state',array($this,'oe_combo_state'));

        add_action('oe_manage_city',array($this,'oe_manage_city'));
        add_action('oe_manage_state',array($this,'oe_manage_state'));
        add_action('oe_manage_country',array($this,'oe_manage_country'));
    }

    public function oe_install() {

        global $wpdb;
        $tablePrefix = $wpdb->prefix;

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

        $sqlCountry = "CREATE TABLE `".$tablePrefix."onde_encontrar_country` (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
                          `active` int(1) DEFAULT NULL,
                          `iso_code` varchar(45) CHARACTER SET latin1 NOT NULL,
                          PRIMARY KEY (`id`)
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8";

        dbDelta( $sqlCountry );

        //State
        $sqlState = "CREATE TABLE `".$tablePrefix."onde_encontrar_state` (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
                          `id_country` int(11) NOT NULL,
                          `active` int(1) DEFAULT NULL,
                          `iso_code` varchar(45) CHARACTER SET latin1 NOT NULL,
                          PRIMARY KEY (`id`)
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8";

        dbDelta( $sqlState );

        $sqlCity = "CREATE TABLE `".$tablePrefix."onde_encontrar_city` (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
                          `id_state` int(11) NOT NULL,
                          `active` int(1) DEFAULT NULL,
                          PRIMARY KEY (`id`)
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        dbDelta( $sqlCity );

        //store
        $sqlStore = "CREATE TABLE `".$tablePrefix."onde_encontrar_store` (
                      `id` int(11) NOT NULL AUTO_INCREMENT,
                      `name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
                      `address` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
                      `complement` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
                      `cep` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
                      `neighborhood` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
                      `email` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
                      `phone` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
                      `id_state` int(11) DEFAULT NULL,
                      `id_city` int(11) DEFAULT NULL,
                      `id_country` int(11) DEFAULT NULL,
                      `latitude` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
                      `active` int(1) DEFAULT NULL,
                      `date_add` datetime DEFAULT NULL,
                      `date_upd` datetime DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        dbDelta( $sqlStore );
    }

    public function admin_menu() {

        add_menu_page(__("Onde Encontrar", $this->slug_plugin), __("Onde Encontrar", $this->slug_plugin), 'manage_options', 'oe_list', array($this,'oe_list'));
        add_submenu_page( $this->menu_slug, __( 'Estados', $this->slug_plugin ),  __( 'Estados', $this->slug_plugin ), 'manage_options', 'oe_list&type=state', array($this,'oe_list') );
        add_submenu_page( $this->menu_slug, __( 'Paises', $this->slug_plugin ), __( 'Paises', $this->slug_plugin ), 'manage_options', 'oe_list&type=country', array($this,'oe_list') );
        add_submenu_page( $this->menu_slug, __( 'Cidades', $this->slug_plugin ),  __( 'Cidades', $this->slug_plugin ), 'manage_options', 'oe_list&type=city', array($this,'oe_list') );

        add_submenu_page(NULL,__( 'Adicionar novo', $this->slug_plugin ), __( 'Adicionar nova loja', $this->slug_plugin ), 'manage_options', 'oe_manage_store', array($this,'oe_manage_store') );
        add_submenu_page(NULL,__( 'Adicionar novo', $this->slug_plugin ), __( 'Adicionar nova loja', $this->slug_plugin ), 'manage_options', 'oe_manage_city', array($this,'oe_manage_city') );
        add_submenu_page(NULL,__( 'Adicionar novo', $this->slug_plugin ), __( 'Adicionar nova loja', $this->slug_plugin ), 'manage_options', 'oe_manage_state', array($this,'oe_manage_state') );
        add_submenu_page(NULL,__( 'Adicionar novo', $this->slug_plugin ), __( 'Adicionar nova loja', $this->slug_plugin ), 'manage_options', 'oe_manage_country', array($this,'oe_manage_country') );
    }

    //include Css
    public function admin_css() {
        wp_enqueue_style('admin_encontrar', plugins_url('/css/onde-encontrar.css', __FILE__));
    }

    //include admin JS
    public function admin_js() {
        wp_enqueue_script('admin_encontrar_js', plugins_url('/js/jquery.maskedinput.min.js', __FILE__), array('jquery'));
        wp_enqueue_script('admin_encontrar_script_js', plugins_url('/js/script.js', __FILE__), array('jquery'));
    }

    public function oe_th($value) {
        echo '<th scope="row">
                    '.$value.'
                </th>';
    }

    public function oe_tdO() {
        echo '<td colspan="1" rowspan="1" >';
    }

    public function oe_tdC() {
        echo '</td>';
    }

    public function oe_trO() {
        echo '<tr>';
    }

    public function oe_trC() {
        echo '</tr>';
    }

    public function oe_tableO() {
        echo '<table class="oe_table form-table" cellspacing="10" cellpadding="1">';
    }

    public function oe_tableC() {
        echo '</table>';
    }

    public function oe_form_store () {

        Global $wpdb;

        $table = $wpdb->prefix.'onde_encontrar_store';
        $options = array();
        $action = isset($_REQUEST['action'])?$_REQUEST['action']:'';
        $stateList = array();
        $cityList = array();

        if($action == 'edit' && intval($_REQUEST['id']) > 0 ){

            $values = array(intval($_REQUEST['id']));

            $sql = "SELECT * FROM $table WHERE id = %s";
            $prepare = $wpdb->prepare($sql,$values);
            $options = $wpdb->get_row($prepare,'ARRAY_A');

            //result rows stores
            $sql = "SELECT * FROM ".$wpdb->prefix."onde_encontrar_state WHERE active = %s AND id = %s";
            $prepare = $wpdb->prepare($sql,array(1,$options['id_state']));
            $stateList = $wpdb->get_results($prepare,'ARRAY_A');

            $sql = "SELECT * FROM ".$wpdb->prefix."onde_encontrar_city WHERE active = %s AND id = %s";
            $prepare = $wpdb->prepare($sql,array(1,$options['id_city']));
            $cityList = $wpdb->get_results($prepare,'ARRAY_A');

        }

        if($_POST) {

            $options = $_POST;
            $table_data = array(
                'name'            => $options['name'],
                'address'          => $options['address'],
                'complement'       => $options['complement'],
                'cep'             => $options['cep'],
                'neighborhood'    => $options['neighborhood'],
                'email'           => $options['email'],
                'phone'           => $options['phone'],
                'id_state'        => $options['id_state'],
                'id_city'         => $options['id_city'],
                'id_country'      => $options['id_country'],
                'latitude'        => $options['latitude'],
                'active'          => $options['active']
            );

            if(isset($_REQUEST['id'])){
                try {
                    $table_data['date_upd'] = date('Y-m-d h:i:s');

                    $wpdb->update($table, $table_data, array('id' => intval($_REQUEST['id'])));
                    do_action( 'oe_sucess');
                }catch (Exception $e){
                    do_action( 'admin_notices');
                }
            }else {
                try{

                    $table_data['date_upd'] = date('Y-m-d h:i:s');
                    $table_data['date_add'] = date('Y-m-d h:i:s');

                    $wpdb->insert($table, $table_data);
                    do_action( 'oe_sucess');
                }catch (Exception $e){
                    do_action( 'admin_notices');
                }
            }
        }

        $sql = "SELECT * FROM ".$wpdb->prefix."onde_encontrar_country WHERE active = %s";
        $prepare = $wpdb->prepare($sql,1);
        $countryList = $wpdb->get_results($prepare,'ARRAY_A');

        echo '
            <div class="wrap">
            <div id="icon-users" class="icon32"><br/></div>
                <h2>'.__('Administrar lojas',$this->slug_plugin).'</h2>
                <div style="background:#ECECEC;border:1px solid #CCC;padding:0 10px;margin-top:5px;border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;">
                    <p>Preencha todos os campos para cadastrar uma nova loja.</p>
                </div>
            <div class="oe_container" >
                <form id="item-filter" method="post" action="">';

        // This prints out all hidden setting fields
        settings_fields('general');

        $this->oe_tableO();
        $this->oe_trO();
        $this->oe_th('<label for="name">Latitude longitude</label>');
        $this->oe_tdO();
        echo '<input type="text" class="oe_input" id="latitude" placeholder="ex: -1215444,-5568655" name="latitude" value="'.(isset($options['latitude'])?$options['latitude']:'').'" />'."<br/>";
        $this->oe_tdC();
        $this->oe_trC();

        $this->oe_trO();
        $this->oe_th('<label for="name">Nome da loja</label>');
        $this->oe_tdO();
        echo '<input type="text" class="oe_input" id="name" placeholder="Nome da Loja" name="name" value="'.(isset($options['name'])?$options['name']:'').'" />'."<br/>";
        $this->oe_tdC();
        $this->oe_trC();
        $this->oe_trO();
        $this->oe_th('<label for="address">Endereço</label>');
        $this->oe_tdO();
        echo '<input type="text" class="oe_input" id="address" placeholder="Endereço" name="address" value="'.(isset($options['address'])?$options['address']:'').'" />'."<br/>";
        $this->oe_tdC();
        $this->oe_trC();
        $this->oe_trO();
        $this->oe_th('<label for="complement">Complemento</label>');
        $this->oe_tdO();
        echo '<input type="text" class="oe_input" id="complement" placeholder="Complemento" name="complement" value="'.(isset($options['complement'])?$options['complement']:'').'" />'."<br/>";
        $this->oe_tdC();
        $this->oe_trC();
        $this->oe_trO();
        $this->oe_th('<label for="cep">Cep</label>');
        $this->oe_tdO();
        echo '<input type="text" class="oe_input" id="cep" placeholder="Cep" name="cep" value="'.(isset($options['cep'])?$options['cep']:'').'" />'."<br/>";
        $this->oe_tdC();
        $this->oe_trC();
        $this->oe_trO();
        $this->oe_th('<label for="neighborhood">Bairro</label>');
        $this->oe_tdO();
        echo '<input type="text" class="oe_input" id="neighborhood" placeholder="Bairro" name="neighborhood" value="'.(isset($options['neighborhood'])?$options['neighborhood']:'').'" />'."<br/>";
        $this->oe_tdC();
        $this->oe_trC();
        $this->oe_trO();
        $this->oe_th('<label for="email">E-mail</label>');
        $this->oe_tdO();
        echo '<input type="text" class="oe_input" id="email" placeholder="Email" name="email" value="'.(isset($options['email'])?$options['email']:'').'" />'."<br/>";
        $this->oe_tdC();
        $this->oe_trC();
        $this->oe_trO();
        $this->oe_th('<label for="phone">Telefone</label>');
        $this->oe_tdO();
        echo '<input type="text" class="oe_input" id="phone" placeholder="phone" name="phone" value="'.(isset($options['phone'])?$options['phone']:'').'" />'."<br/>";
        $this->oe_tdC();
        $this->oe_trC();
        $this->oe_trO();
        $this->oe_th('<label for="is_country">País</label>');
        $this->oe_tdO();
        echo '<select id="id_country" class="oe_select auto-carregar" name="id_country"  data-url="admin-ajax.php?action=oe_combo_state" data-combo-id="id_state" data-combo-valor="id" data-combo-label="name" data-selected="'.(isset($options['id_country'])?$options['id_country']:'').'">
                                    <option >Selecione o pais</option>';
        foreach ($countryList as $country):
            echo '<option value="'.$country['id'].'" '.(isset($options['id_country'])?($options['id_country'] == $country['id'] ?'selected="selected"':''):'').'>'.$country['name'].' - '.$country['iso_code'].'</option>';
        endforeach;
        echo '</select>'."<br/>";
        $this->oe_tdC();
        $this->oe_trC();
        $this->oe_trO();
        $this->oe_th('<label for="state">Estado</label>');
        $this->oe_tdO();
        echo '<select id="id_state" class="oe_select auto-carregar" name="id_state"  data-url="admin-ajax.php?action=oe_combo_city" data-combo-id="id_city" data-combo-valor="id" data-combo-label="name" data-selected="'.(isset($options['id_state'])?$options['id_state']:'').'">
                        <option >Selecione o estado</option>';

        foreach ($stateList as $state):
            echo '<option value="'.$state['id'].'" '.(isset($options['id_state'])?($options['id_state'] == $state['id'] ?'selected="selected"':''):'').'>'.$state['name'].' - '.$state['iso_code'].'</option>';
        endforeach;

        echo '</select>'."<br/>";
        $this->oe_tdC();
        $this->oe_trC();
        $this->oe_trO();
        $this->oe_th('<label for="city">Cidade</label>');
        $this->oe_tdO();
        echo '<select id="id_city" class="oe_select"  name="id_city" data-selected="'.(isset($options['id_state'])?$options['id_state']:'').'">
                            <option >Selecione a cidade</option>';
        foreach ($cityList as $city):
            echo '<option value="'.$city['id'].'" '.(isset($options['id_city'])?($options['id_city'] == $city['id'] ?'selected="selected"':''):'').'>'.$city['name'].'</option>';
        endforeach;

        echo '</select>'."<br/>";
        $this->oe_tdC();
        $this->oe_trC();

        $this->oe_trO();
        $this->oe_th('<label for="ativo">Ativo:</label>');
        $this->oe_tdO();
        echo '<select id="active" class="oe_select" name="active">
                                    <option >Selecione o status</option>
                                    <option value="1" '.(isset($options['active'])?($options['active'] ==1 ?'selected="selected"':''):'').'>Ativo</option>
                                    <option value="0" '.(isset($options['active'])?($options['active'] !=1 ?'selected="selected"':''):'').'>Inativo</option>
                                </select>'."<br/>";
        $this->oe_tdC();
        $this->oe_trC();




        $this->oe_tableC();


        submit_button();
        echo '
                </form>
            </div>
            </div>';

    }

    public function oe_form_city () {
        Global $wpdb;

        $table = $wpdb->prefix.'onde_encontrar_city';
        $action = isset($_REQUEST['action'])?$_REQUEST['action']:'';

        $sql = "SELECT * FROM ".$wpdb->prefix."onde_encontrar_state WHERE active = %s";
        $prepare = $wpdb->prepare($sql,1);
        $stateList = $wpdb->get_results($prepare,'ARRAY_A');

        if(isset($_REQUEST['id']) && $action == 'edit') {

            $sql = "SELECT * FROM $table WHERE id = '%s'";
            $prepare = $wpdb->prepare($sql,$_REQUEST['id']);
            $data = $wpdb->get_row($prepare,ARRAY_A);

        }

        if($_POST) {
            $data = $_POST;

            $table_data = array(
                'name'      => $data['name'],
                'id_state'  => $data['id_state'],
                'active'    => intval($data['active'])
            );

            if(isset($_REQUEST['id'])){
                try {
                    $wpdb->update($table, $table_data, array('id' => intval($_REQUEST['id'])));
                    do_action( 'oe_sucess');
                }catch (Exception $e){
                    do_action( 'admin_notices');
                }
            }else {
                try{
                    $wpdb->insert($table, $table_data);
                    do_action( 'oe_sucess');
                }catch (Exception $e){
                    do_action( 'admin_notices');
                }
            }
        }

        echo '
           <div class="wrap">
            <div id="icon-users" class="icon32"><br/></div>
                <h2>'.__('Administrar estados',$this->slug_plugin).'</h2>
                <div style="background:#ECECEC;border:1px solid #CCC;padding:0 10px;margin-top:5px;border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;">
                    <p>Preencha todos os campos para cadastrar um novo estado.</p>
                </div>
            <div class="oe_container" >
            <form method="post" action="">';

        // This prints out all hidden setting fields
        settings_fields( 'general' );

        $this->oe_tableO();
        $this->oe_trO();
        $this->oe_th('<label for="name">Nome da cidade:</label>');
        $this->oe_tdO();
        echo '<input type="text" class="oe_input" id="name" placeholder="Nome da Cidade" name="name" value="'.(isset($data['name'])?$data['name']:'').'" />'."<br/>";
        $this->oe_tdC();
        $this->oe_trC();

        $this->oe_trO();
        $this->oe_th('<label for="id_state">Estado</label>');
        $this->oe_tdO();
        echo '<select id="id_state" class="oe_select" name="id_state"  data-url="admin-ajax.php?action='.$_REQUEST['page'].'">
                 <option >Selecione o estado</option>';
        foreach ($stateList as $state):
            echo '<option value="'.$state['id'].'" '.(isset($data['id_state'])?($data['id_state'] ==$state['id'] ?'selected="selected"':''):'').'>'.$state['name'].'</option>';
        endforeach;
        echo '</select>'."<br/>";
        $this->oe_tdC();
        $this->oe_trC();

        $this->oe_trO();
        $this->oe_th('<label for="ativo">Ativo:</label>');
        $this->oe_tdO();
        echo '<select id="active" class="oe_select" name="active">
                            <option >Selecione o status</option>
                            <option value="1" '.(isset($data['active'])?($data['active'] ==1 ?'selected="selected"':''):'').'>Ativo</option>
                            <option value="0" '.(isset($data['active'])?($data['active'] !=1 ?'selected="selected"':''):'').'>Inativo</option>
                        </select>'."<br/>";
        $this->oe_tdC();
        $this->oe_trC();

        $this->oe_tableC();
        submit_button();
        echo '
            </form>
            </div>
            </div>';

    }

    public function oe_manage_state () {
        Global $wpdb;

        $table = $wpdb->prefix.'onde_encontrar_state';
        $action = isset($_REQUEST['action'])?$_REQUEST['action']:'';

        $sql = "SELECT * FROM ".$wpdb->prefix."onde_encontrar_country WHERE active = %s";
        $prepare = $wpdb->prepare($sql,1);
        $countryList = $wpdb->get_results($prepare,'ARRAY_A');

        if(isset($_REQUEST['id']) && $action == 'edit') {

            $sql = "SELECT * FROM $table WHERE id = '%s'";
            $prepare = $wpdb->prepare($sql,$_REQUEST['id']);
            $data = $wpdb->get_row($prepare,ARRAY_A);

        }

        if($_POST) {
            $data = $_POST;

            $table_data = array(
                'name'      => $data['name'],
                'iso_code'  => $data['iso_code'],
                'id_country'  => $data['id_country'],
                'active'    => intval($data['active'])
            );

            if(isset($_REQUEST['id'])){
                try {
                    $wpdb->update($table, $table_data, array('id' => intval($_REQUEST['id'])));
                    do_action( 'oe_sucess');
                }catch (Exception $e){
                    do_action( 'admin_notices');
                }
            }else {
                try{
                    $wpdb->insert($table, $table_data);
                    do_action( 'oe_sucess');
                }catch (Exception $e){
                    do_action( 'admin_notices');
                }
            }
        }




        echo '
           <div class="wrap">
            <div id="icon-users" class="icon32"><br/></div>
                <h2>'.__('Administrar estados',$this->slug_plugin).'</h2>
                <div style="background:#ECECEC;border:1px solid #CCC;padding:0 10px;margin-top:5px;border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;">
                    <p>Preencha todos os campos para cadastrar um novo estado.</p>
                </div>
            <div class="oe_container" >
            <form method="post" action="">';

        // This prints out all hidden setting fields
        settings_fields( 'general' );

        $this->oe_tableO();
        $this->oe_trO();
        $this->oe_th('<label for="name">Nome do Estado:</label>');
        $this->oe_tdO();
        echo '<input type="text" class="oe_input" id="name" placeholder="Nome da Estado" name="name" value="'.(isset($data['name'])?$data['name']:'').'" />'."<br/>";
        $this->oe_tdC();
        $this->oe_trC();

        $this->oe_trO();
        $this->oe_th('<label for="name">Sigla:</label>');
        $this->oe_tdO();
        echo '<input type="text" class="oe_input" id="iso_code" placeholder="ex: RS" name="iso_code" value="'.(isset($data['iso_code'])?$data['iso_code']:'').'" />'."<br/>";
        $this->oe_tdC();
        $this->oe_trC();

        $this->oe_trO();
        $this->oe_th('<label for="is_country">País</label>');
        $this->oe_tdO();
        echo '<select id="id_country" class="oe_select auto-carregar" name="id_country"  data-url="admin-ajax.php?action='.$_REQUEST['page'].'" data-combo-id="id_state" data-combo-valor="id" data-combo-label="name" data-selected="'.(isset($options['id_country'])?$options['id_country']:'').'">
                 <option >Selecione o país</option>';
        foreach ($countryList as $country):
            echo '<option value="'.$country['id'].'" '.(isset($data['id_country'])?($data['id_country'] ==$country['id'] ?'selected="selected"':''):'').'>'.$country['name'].' - '.$country['iso_code'].'</option>';
        endforeach;
        echo '</select>'."<br/>";
        $this->oe_tdC();
        $this->oe_trC();

        $this->oe_trO();
        $this->oe_th('<label for="ativo">Ativo:</label>');
        $this->oe_tdO();
        echo '<select id="active" class="oe_select" name="active">
                            <option >Selecione o status</option>
                            <option value="1" '.(isset($data['active'])?($data['active'] ==1 ?'selected="selected"':''):'').'>Ativo</option>
                            <option value="0" '.(isset($data['active'])?($data['active'] !=1 ?'selected="selected"':''):'').'>Inativo</option>
                        </select>'."<br/>";
        $this->oe_tdC();
        $this->oe_trC();

        $this->oe_tableC();


        submit_button();
        echo '
            </form>
            </div>
            </div>';

    }

    public function oe_manage_country () {

        Global $wpdb;

        $table = $wpdb->prefix.'onde_encontrar_country';
        $action = isset($_REQUEST['action'])?$_REQUEST['action']:'';

        if(isset($_REQUEST['id']) && $action == 'edit') {

            $sql = "SELECT * FROM $table WHERE id = '%s'";
            $prepare = $wpdb->prepare($sql,$_REQUEST['id']);
            $data = $wpdb->get_row($prepare,ARRAY_A);

        }

        if($_POST) {
            $data = $_POST;

            $table_data = array(
                'name'      => $data['name'],
                'iso_code'  => $data['iso_code'],
                'active'    => intval($data['active'])
            );

            if(isset($_REQUEST['id'])){
                try {
                    $wpdb->update($table, $table_data, array('id' => intval($_REQUEST['id'])));
                    do_action( 'oe_sucess');
                }catch (Exception $e){
                    do_action( 'admin_notices');
                }
            }else {
                try{
                    $wpdb->insert($table, $table_data);
                    do_action( 'oe_sucess');
                }catch (Exception $e){
                    do_action( 'admin_notices');
                }
            }
        }


        echo '
           <div class="wrap">
            <div id="icon-users" class="icon32"><br/></div>
                <h2>'.__('Administrar paises',$this->slug_plugin).' </h2>
                <div style="background:#ECECEC;border:1px solid #CCC;padding:0 10px;margin-top:5px;border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;">
                    <p>Preencha todos os campos para cadastrar um novo país.</p>
                </div>
            <div class="oe_container" >
            <form method="post" action="">';

        // This prints out all hidden setting fields
        settings_fields( 'general' );

        $this->oe_tableO();

        $this->oe_trO();
        $this->oe_th('<label for="name">Nome do Estado:</label>');
        $this->oe_tdO();
        echo '<input type="text" class="oe_input" id="name" placeholder="Nome do pais" name="name" value="'.(isset($data['name'])?$data['name']:'').'" />'."<br/>";
        $this->oe_tdC();
        $this->oe_trC();

        $this->oe_trO();
        $this->oe_th('<label for="name">Sigla:</label>');
        $this->oe_tdO();
        echo '<input type="text" class="oe_input" id="iso_code" placeholder="ex: BR" name="iso_code" value="'.(isset($data['iso_code'])?$data['iso_code']:'').'" />'."<br/>";
        $this->oe_tdC();
        $this->oe_trC();

        $this->oe_trO();
        $this->oe_th('<label for="ativo">Ativo:</label>');
        $this->oe_tdO();
        echo '<select id="active" class="oe_select" name="active">
                                <option >Selecione o status</option>
                                <option value="1" '.(isset($data['active'])?($data['active'] ==1 ?'selected="selected"':''):'').'>Ativo</option>
                                <option value="0" '.(isset($data['active'])?($data['active'] !=1 ?'selected="selected"':''):'').'>Inativo</option>
                            </select>'."<br/>";
        $this->oe_tdC();
        $this->oe_trC();

        $this->oe_tableC();


        submit_button();
        echo '
            </form>
            </div>
            </div>';

    }

    public function oe_list() {

        require_once( plugin_dir_path( __FILE__ ).'inc/class.list.php' );

        $table = new table_onde_encontrar();
        $type = isset($_REQUEST['type'])?$_REQUEST['type']:'';

        switch ($type) {
            case 'city':

                $table->table = 'city';

                //colunas da listagem
                $table->columns = array(
                    'cb' => '<input type="checkbox" />', //Render a checkbox instead of text
                    'name' => 'Nome',
                    'id_state' => 'Estado',
                    'active' => 'Status',
                );

                //colunas que podem ser filtradas
                $table->sortable_columns = array(
                    'id' => array('id', false),     //true means it's already sorted
                    'name' => array('name', false),
                    'id_state' => array('id_state', false)
                );
                break;
            case 'state':
                $table->table = 'state';

                //colunas da listagem
                $table->columns = array(
                    'cb' => '<input type="checkbox" />', //Render a checkbox instead of text
                    'name' => 'Nome',
                    'id_country' => 'País',
                    'active' => 'Status',
                );

                //colunas que podem ser filtradas
                $table->sortable_columns = array(
                    'id' => array('id', false),     //true means it's already sorted
                    'name' => array('name', false),
                    'id_country' => array('id_country', false)
                );
                break;
            case 'country':
                $table->table = 'country';

                //colunas da listagem
                $table->columns = array(
                    'cb' => '<input type="checkbox" />', //Render a checkbox instead of text
                    'name' => 'Nome',
                    'active' => 'Status',
                );

                //colunas que podem ser filtradas
                $table->sortable_columns = array(
                    'id' => array('id', false),     //true means it's already sorted
                    'name' => array('name', false),
                );
                break;
            default:

                $type = 'store';
                $table->table = 'store';

                //colunas da listagem
                $table->columns = array(
                    'cb' => '<input type="checkbox" />', //Render a checkbox instead of text
                    'name' => 'Nome',
                    'address' => 'Endereço',
                    'active' => 'Status',
                    'date_add' => 'Data Adicionado'
                );

                //colunas que podem ser filtradas
                $table->sortable_columns = array(
                    'id' => array('id', false),     //true means it's already sorted
                    'name' => array('name', false),
                    'date_add' => array('date_add', false)
                );
        }
        $table->prepare_items();

        ?>
        <div class="wrap">
            <div id="icon-users" class="icon32"><br/></div>
            <h2>Lojas cadastradas</h2><a href="<?php echo esc_url( admin_url("admin.php?page=oe_manage_{$type}&action=new") )?>" class="page-title-action">Adicionar novo</a>
            <div style="background:#ECECEC;border:1px solid #CCC;padding:0 10px;margin-top:5px;border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;">
                <p>Aqui você encontra a lista de todas as lojas cadastradas para onde encontrar.</p>
            </div>
            <form id="item-filter" method="get">
                <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                <?php $table->display() ?>
            </form>
        </div>
        <?php
    }

    public function oe_notice__success() {

        $class = 'notice notice-success is-dismissible';
        $message = __( 'Item foi salvo com sucesso! ', $this->slug_plugin );

        printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message);

    }

    public function oe_notice__error() {

        $class = 'notice notice-error';
        $message = __( 'Ocorreu um erro: ', $this->slug_plugin );

        printf( '<div class="%1$s"><p>%2$s %3$s</p></div>', $class, $message,$this->message );
    }

    public function oe_combo_city() {

        Global $wpdb;

        if(intval($_POST['id']) <= 0 ){
            return;
        }

        $table = $wpdb->prefix."onde_encontrar_city";

        $sql = "SELECT * FROM $table WHERE id_state = '%s'";
        $prepare = $wpdb->prepare($sql,$_REQUEST['id']);
        $data = $wpdb->get_results($prepare,ARRAY_A);

        $json = new stdClass();
        $json->colecao = $data;
        $json->erro = 0;

        die(json_encode($json));
    }

    public function oe_combo_state() {

        Global $wpdb;

        if(intval($_POST['id']) <= 0 ){
            print_r($_POST);
            die('erro');
        }

        $table = $wpdb->prefix."onde_encontrar_state";

        $sql = "SELECT * FROM $table WHERE id_country = '%s'";
        $prepare = $wpdb->prepare($sql,$_REQUEST['id']);
        $data = $wpdb->get_results($prepare,ARRAY_A);

        $json = new stdClass();
        $json->erro = 0;
        $json->colecao = $data;

        die(json_encode($json));
    }

    public function oe_combo_list_store () {

        Global $wpdb;

        if(intval($_POST['id_state']) <= 0 AND intval($_POST['id_city']) <= 0){
            wp_die('Ocorreu um erro inesperado');
        }

        $table = $wpdb->prefix."onde_encontrar_store";
        $ids = array($_POST['id_state'],$_POST['id_city']);

        $sql = "SELECT store.*,state.name as StateName, city.name as CityName FROM $table store
                    LEFT JOIN ". $wpdb->prefix . "onde_encontrar_state state ON store.id_state = state.id
                    LEFT JOIN ". $wpdb->prefix . "onde_encontrar_city city ON store.id_city = city.id
                    WHERE store.id_state = '%s' AND store.id_city = '%s'";

        $prepare = $wpdb->prepare($sql,$ids);
        $data = $wpdb->get_results($prepare,ARRAY_A);

        $json = new stdClass();
        $json->erro = 0;

        if(count($data) > 0) {
            $json->colecao = $data;
        }else{
            $json->message = __('Não foi encontrada nenhuma loja',$this->slug_plugin);
        }


        die(json_encode($json));
    }

    public function oe_shortcode() {

        Global $wpdb;

        if(is_page()){

            $table = $wpdb->prefix.'onde_encontrar_state';

            wp_enqueue_script('google-maps', 'https://maps.googleapis.com/maps/api/js?key='.$this->apiKeyGoogle.'&sensor=false',array('jquery'),false,false);
            wp_enqueue_script('google-jsapi','https://www.google.com/jsapi');

            wp_enqueue_style('admin_encontrar', plugins_url('front/css/onde-encontrar.css', __FILE__));
            wp_enqueue_script('admin_encontrar_front_script_js', plugins_url('front/js/script.js', __FILE__), array('jquery'));

            $sql = "SELECT * FROM $table WHERE id_country = '%s'";
            $prepare = $wpdb->prepare($sql,1);
            $stateList = $wpdb->get_results($prepare,ARRAY_A);

            $BASE_URL = plugin_dir_url(__FILE__);
            include_once(plugin_dir_path(__FILE__).'front/front-html.php');
        }


    }

}

$oe = new Admin();