<script>
    var BASE_URL = '<?php echo $BASE_URL; ?>';
</script>

<div class="oe_wrap">
    <div class="oe_container">
        <div class="oe_gmaps" id="oe_gmaps"></div> <!--google maps -->

        <h1>Lojas Mormaii</h1>

        <form action="<?php echo admin_url();?>admin-ajax.php?action=oe_combo_store" method="post" class="oe_form">

            <input type="hidden" name="id_country" id="id_country" value="1">

                <select id="id_state" class="oe_select auto-carregar" name="id_state"  data-combo-id="id_city" data-combo-valor="id" data-combo-label ="name" data-url="<?php echo admin_url();?>admin-ajax.php?action=oe_combo_city">
                    <option ><?php echo __('Selectione um estado:','oe_lojas');?></option>
                    <?php
                        foreach ($stateList as $state):
                        echo '<option value="'.$state['id'].'">'.$state['name'].'</option>';
                        endforeach;
                    ?>
                </select>

                <select id="id_city" class="oe_select" name="id_city">
                    <option ><?php echo __('Selectione uma cidade:','oe_lojas');?></option>
                    <?php
                        foreach ($cityList as $city):
                            echo '<option value="'.$city['id'].'">'.$city['name'].'</option>';
                        endforeach;
                    ?>
                </select>

            <button class="oe_submit" type="submit"><?php echo __('Buscar lojas','oe_lojas');?></button>
        </form>
        <div class="oe_result">

            <ul class="oe_ul" >
                <li class="oe_ul_li" style="display:none">
                    <img src="" class="oe_image">
                    <div class="oe_info">
                        <h2 class="oe_name"></h2>
                        <div class="oe_endereco"><strong><?php echo __( 'Endereço', 'oe_list' )?>:</strong> </div>
                        <div class="oe_cidade"><strong><?php echo __( 'Cidade/Estado', 'oe_list' )?>:</strong> </div>
                        <div class="oe_cep"><strong><?php echo __( 'Cep', 'oe_list' )?>:</strong> </div>
                        <div class="oe_email"><strong><?php  echo __( 'E-mail', 'oe_list' )?>:</strong> </div>
                        <div class="oe_telefone"><strong><?php echo __( 'Telefone', 'oe_list' )?>:</strong> </div>
                        <a class="oe_vermais"><?php echo __( 'Ver mais', 'oe_list' )?></a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>