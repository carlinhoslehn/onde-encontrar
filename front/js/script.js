(function($){
    var map;

    function ininialize () {
        map = new google.maps.Map(document.getElementById('oe_gmaps'), {
            center: {lat: -29.9849516, lng: -53.839734},
            zoom: 7
        });
    }

    $(document).ready(function(){

        ininialize();
        $('.oe_form').on('submit',function(e) {

            e.preventDefault();
            var url = $(this).attr('action');
            $.ajax({
                url: url,
                type: "POST",
                dataType: 'json',
                data:$(this).serialize(),
                success: function(json) {
                    if(json.erro === 0) {
                        var colecao = json.colecao;
                        for(var indice in colecao) {

                            var latitude = colecao[indice]['latitude'];
                            var latitudeSplit = latitude.split(',')
                            var marker = new google.maps.Marker({
                                map: map,
                                position: new google.maps.LatLng(parseFloat(latitudeSplit[0]), parseFloat(latitudeSplit[1])),
                                title: colecao[indice]['name'],
                                icon: BASE_URL+'/front/img/pin.png'
                            });

                            if(indice == 0) {
                                map.setCenter({lat: parseFloat(latitudeSplit[0]), lng: parseFloat(latitudeSplit[1])});
                                map.setZoom(10);
                            }

                            item = $('.oe_result ul li:first').clone();
                            item.show();
                            item.find('.oe_vermais').attr('latitude',colecao[indice]['latitude']);
                            item.find('.oe_name').append(colecao[indice]['name']);
                            item.find('.oe_endereco').append(colecao[indice]['address']);
                            item.find('.oe_cidade').append(colecao[indice]['StateName']+' / ' + colecao[indice]['CityName']);
                            item.find('.oe_cep').append(colecao[indice]['cep']);
                            item.find('.oe_email').append(colecao[indice]['email']);
                            item.find('.oe_telefone').append(colecao[indice]['phone']);
                            item.appendTo('.oe_result ul');

                        }
                    } else if(json.erro === 1) {
                        alert(json.mensagem);
                    }
                },
                error: function () {
                }
            });

        });

        $('.oe_vermais').on('click', function(evt) {

            evt.preventDefault();
            var data = $(this).data();
            var indexUnidade = split(',',data.latitude);
            map.setCenter({lat: indexUnidade[0], lng: indexUnidade[1]});
            map.setZoom(17);

        });



        $('.auto-carregar').change(function() {
            var elem = $(this);
            var combo = $('#'+elem.data('combo-id'));
            var comboValor = elem.data('combo-valor');
            var comboLabel = elem.data('combo-label');
            var url = elem.data('url');
            var id = $('option:selected', elem).val();

            if(id != '') {
                combo.html('<option value="">Carregando...</option>');
                combo.change();
                $.ajax({
                    url: url,
                    type: "POST",
                    dataType: 'json',
                    data:{id:id},
                    success: function(json) {
                        var html = '<option value="">Selecione</option>';
                        if(json.erro === 0) {
                            var colecao = json.colecao;
                            for(var indice in colecao) {
                                html += '<option value="'+colecao[indice][comboValor]+'">'+colecao[indice][comboLabel]+'</option>';
                            }
                        } else if(json.erro === 1) {
                            alert(json.mensagem);
                        }
                        combo.html(html);
                        combo.change();
                    },
                    error: function () {
                        combo.html('<option value="">Selecione</option>');
                        combo.change();
                    }
                });
                combo.val(id);
            }
        });
     });
})(jQuery);