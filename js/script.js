(function($){


    $(document).ready(function(){
        $('#cep').mask('99.999-999?9');
        //$('#phone').mask('(99) 9999-9999?9');



        $('.auto-carregar').change(function() {
            var elem = $(this);
            var combo = $('#'+elem.data('combo-id'));
            var comboValor = elem.data('combo-valor');
            var comboLabel = elem.data('combo-label');
            var url = elem.data('url');
            var id = $('option:selected', elem).val();

            if(id != '') {
                combo.html('<option value="">Carregando...</option>');
                combo.change();
                $.ajax({
                    url: url,
                    type: "POST",
                    dataType: 'json',
                    data:{id:id},
                    success: function(json) {
                        var html = '<option value="">Selecione</option>';
                        if(json.erro === 0) {
                            var colecao = json.colecao;
                            for(var indice in colecao) {
                                html += '<option value="'+colecao[indice][comboValor]+'">'+colecao[indice][comboLabel]+'</option>';
                            }
                        } else if(json.erro === 1) {
                            alert(json.mensagem);
                        }
                        combo.html(html);
                        combo.change();
                    },
                    error: function () {
                        combo.html('<option value="">Selecione</option>');
                        combo.change();
                    }
                });
                combo.val(id);
            }
        });
     });
})(jQuery);